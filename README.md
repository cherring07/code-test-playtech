# tweet-search

tweet-search uses Create React App as an initial base for the App, and then customised with a number packages providing things like linting, testing, state management, and styling. I spent around 90mins over a couple days on the test and unfortunately did not get round to integrating the AUTH and subsequence API's for twitter. 

Key design decisions 
1) URL updates on search to provide, to provide linkable search results
2) Container vs Presentational Components approach. 