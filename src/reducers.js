import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import searchReducer from './containers/Search/reducer';

const rootReducer = combineReducers({
  searchReducer,
  form,
});

export default rootReducer;
