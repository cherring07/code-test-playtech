export const COLOUR_BRAND = '#71C21D';
export const COLOUR_BRAND_DARK = '#538F15';
export const COLOUR_BRAND_LIGHT = '#8EC257';

export const COLOUR_WHITE = '#FFFFFF';
export const COLOUR_BLACK = '#030303';
export const COLOUR_RED = '#F36D69';
export const COLOUR_ORANGE = '#FFC768';
export const COLOUR_YELLOW = '#FFEF1A';
export const COLOUR_GREEN = '#76D276';
export const COLOUR_BLUE = '#1C8FF2';
export const COLOUR_TEAL = '#75DAF8';
export const COLOUR_PINK = '#FF7591';
export const COLOUR_PURPLE = '#7B5796';
export const COLOUR_GREY = '#D1D1D1';
export const COLOUR_GREY_LIGHT = '#DEDEDE';

export const BORDER_WIDTH = '0.1rem';

// Default values
export const theme = {
  fontSizes: [
    12, 14, 16, 24, 32, 48, 64
  ],
  colors: {
    COLOUR_RED,
  },
  buttons: {
    primary: {
      color: '#fff',
      backgroundColor: COLOUR_RED,
    },
    outline: {
      color: COLOUR_RED,
      backgroundColor: 'transparent',
      boxShadow: 'inset 0 0 2px'
    },
  },
}
