import { connect } from 'react-redux';
import Search from './Search';
import { searchRequested } from './actions';

const mapStateToProps = ({ searchReducer: { searchQuery } }) => ({
  searchQuery,
});

const mapDispatchToProps = dispatch => ({
  searchRequested: query => dispatch(searchRequested(query)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
