import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import SearchBar from '../../components/SearchBar';
import TweetCard from '../../components/TweetCard';

import tweets from './tweets';

class Search extends Component {
  onSearchBarChange({ target }) {
    if (target) {
      this.props.history.push(`/search/${target.value}`);
    }
  }

  componentDidUpdate(prevProps) {
    const oldSearchQuery = prevProps.match.params.searchQuery;
    const newSearchQuery = this.props.match.params.searchQuery;
    if (newSearchQuery !== oldSearchQuery) {
      this.props.searchRequested(newSearchQuery);
    }
  }

  render() {
    const { searchQuery } = this.props;

    const tweetCards = tweets.map(tweet => (
      <TweetCard tweet={tweet} key={tweet.id} />
    ));

    return (
      <div>
        <SearchBar onChange={event => this.onSearchBarChange(event)} />

        {searchQuery && (
          <Fragment>
            <h1>{searchQuery}</h1>
            {tweetCards}
          </Fragment>
        )}
      </div>
    );
  }
}

Search.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object,
  searchQuery: PropTypes.string,
  searchRequested: PropTypes.func,
};

export default Search;
