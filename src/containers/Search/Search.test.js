import React from 'react';
import { shallow } from 'enzyme';

import Search from './index';
import Provider from '../../internal/Provider';

describe('<Search />', () => {
  it('displays Search', () => {
    const props = {
      history: {
        push: jest.fn(),
      },
    };

    const wrapper = shallow(
      <Provider>
        <Search {...props} />
      </Provider>
    );

    expect(wrapper.find(Search)).toHaveLength(1);
  });
});
