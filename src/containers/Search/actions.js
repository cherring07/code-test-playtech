import { SEARCH_REQUESTED } from './constants';

export function searchRequested(searchQuery) {
  return {
    type: SEARCH_REQUESTED,
    searchQuery,
  };
}
