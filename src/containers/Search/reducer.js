import { SEARCH_REQUESTED } from './constants';

export const initialState = {
  searchQuery: '',
};

function searchReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_REQUESTED:
      return {
        ...state,
        searchQuery: action.searchQuery,
      };
    default:
      return state;
  }
}

export default searchReducer;
