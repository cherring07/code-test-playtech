import React from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';

import InputText from '../InputText';

const SearchBar = ({
  onChange,
}) => (
  <form>
    <Field
      onChange={onChange}
      component={InputText}
      id="Search"
      label="Search"
      name="search"
      placeholder="Search..."
      type="text"
    />
  </form>
);

SearchBar.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
};

export default reduxForm({
  form: 'formSearch',
})(SearchBar);
