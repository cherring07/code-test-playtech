/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';

const Label = ({
  htmFor,
  text,
}) => (
  <label htmlFor={htmFor}>{text}</label>
);

Label.propTypes = {
  htmFor: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Label;
/* eslint-enable jsx-a11y/label-has-for */
