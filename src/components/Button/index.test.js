import React from 'react';
import { shallow } from 'enzyme';
import Button from './index';

describe('<Button />', () => {
  it('displays Button', () => {
    const wrapper = shallow(<Button text="Submit" />);

    expect(wrapper.html().indexOf('Submit')).not.toBe(-1);
  });
});
