import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import {
  COLOUR_BRAND,
  COLOUR_WHITE,
} from '../../theme';

const CustomButton = styled.button`
  background-color: white;
  border-radius: 0;
  border: 0.2rem solid ${COLOUR_BRAND};
  color: ${COLOUR_BRAND};
  cursor: pointer;
  margin: 0;
  padding: 16px;
  text-align: center;
  transition: 0.3s ease-in-out;
  width: 100%;
  font-size: 1rem;

  :hover {
    background-color: ${COLOUR_BRAND};
    box-shadow: none;
    color: ${COLOUR_WHITE};
  }

  :focus {
    background-color: ${COLOUR_BRAND};
    box-shadow: 0 0 0 0.2rem ${COLOUR_BRAND};
    color: ${COLOUR_WHITE};
  }`;

const Button = ({
  disabled,
  onClick,
  text,
}) => (
  <CustomButton disabled={disabled} onClick={onClick}>
    {text}
  </CustomButton>
);

Button.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  text: PropTypes.string.isRequired,
};

export default Button;
