import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Box, Flex } from 'rebass';

import InputErrorMessage from '../InputErrorMessage';
import Label from '../Label';

import {
  BORDER_WIDTH,
  COLOUR_BLACK,
  COLOUR_BRAND,
  COLOUR_GREY,
  COLOUR_GREY_LIGHT,
  COLOUR_WHITE,
} from '../../theme';

const CustomInput = styled.input`
  background-clip: padding-box;
  background-color: ${COLOUR_WHITE};
  border-radius: 0;
  border: ${BORDER_WIDTH} solid ${COLOUR_GREY};
  caret-color: ${COLOUR_BRAND};
  color: ${COLOUR_BLACK};
  display: block;
  line-height: 1.25;
  padding: 0.5rem 0.75rem;
  transition: 0.3s ease-in-out;
  width: 100%;

  :focus {
    box-shadow: none;
    border: 0.1rem solid ${COLOUR_BRAND};
  }

  ::placeholder {
    /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: ${COLOUR_GREY_LIGHT};
    opacity: 1; /* Firefox */
  }

  :-ms-input-placeholder {
    /* Internet Explorer 10-11 */
    color: ${COLOUR_GREY_LIGHT};
  }

  ::-ms-input-placeholder {
    /* Microsoft Edge */
    color: ${COLOUR_GREY_LIGHT};
  }
`;

const InputText = ({
  input,
  label,
  meta: { touched, error },
  onChange,
  placeholder,
  type,
}) => (
  <Flex flexWrap="wrap">
    <Box pt={3} width={[1, 1, 1 / 3, 1 / 3]}>
      <Label htmFor={input.name} text={label} />
    </Box>

    <Box py={[0, 0, 3]} width={[1, 1, 2 / 3, 2 / 3]}>
      <Flex>
        <CustomInput
          onChange={onChange}
          id={input.name}
          name={input.name}
          placeholder={placeholder}
          type={type}
          {...input}
        />
      </Flex>
      {touched && error && <InputErrorMessage error={error} />}
    </Box>
  </Flex>
);

InputText.propTypes = {
  extra: PropTypes.string,
  input: PropTypes.object,
  label: PropTypes.string.isRequired,
  meta: PropTypes.object,
  onChange: PropTypes.func,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

export default InputText;
