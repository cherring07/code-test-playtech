import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { COLOUR_RED } from '../../theme';

const InputErrorMessage = ({ error }) => {
  const ErrorMessage = styled.div`
  color: ${COLOUR_RED};
  padding-top: 0.5rem;
  padding-left: 0.5rem;
  font-size: 0.75rem;
  line-height: 1rem;
`;

  return (
    <ErrorMessage>
      {error}
    </ErrorMessage>
  );
};

InputErrorMessage.propTypes = {
  error: PropTypes.string.isRequired,
};

export default InputErrorMessage;
