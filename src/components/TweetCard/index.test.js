import React from 'react';
import { shallow } from 'enzyme';
import TweetCard from './index';

describe('<TweetCard />', () => {
  it('displays TweetCard', () => {
    const tweet = {
      text: 'Hello World!',
    };

    const wrapper = shallow(<TweetCard tweet={tweet} />);

    expect(wrapper.html().indexOf('Hello World!')).not.toBe(-1);
  });
});
