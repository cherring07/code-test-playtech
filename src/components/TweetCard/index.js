import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { COLOUR_BRAND, COLOUR_WHITE } from '../../theme';

const CustomTweetCard = styled.div`
  background-color: white;
  border-radius: 0;
  box-shadow: 0 3px 6px 0 hsla(0, 0%, 0%, 0.3);
  cursor: pointer;
  font-size: 1rem;
  margin: 0;
  padding: 16px;
  text-align: left;
  transition: 0.3s ease-in-out;
  width: 100%;
  margin-top: 2rem;
  margin-bottom: 2rem;

  :hover {
    background-color: ${COLOUR_BRAND};
    box-shadow: none;
    color: ${COLOUR_WHITE};
  }
`;

const TweetCard = ({ tweet }) => (
  <CustomTweetCard>{tweet.text}</CustomTweetCard>
);

TweetCard.propTypes = {
  tweet: PropTypes.object,
};

export default TweetCard;
