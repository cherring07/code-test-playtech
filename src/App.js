import React from 'react';
import PropTypes from 'prop-types';
import { ConnectedRouter } from 'connected-react-router';
import { Route, Switch } from 'react-router-dom';

import GlobalStyle from './styles';
import Search from './containers/Search';
import styled from 'styled-components';

const Wrapper = styled.div`
  backgoundcolor: lightgrey;
  max-width: 600px;
  margin: auto;
  height: 100%;
`;

const App = ({ history }) => {
  return (
    <ConnectedRouter history={history}>
      <Wrapper>
        <Switch>
          <Route exact path="/search/:searchQuery" component={Search} />
          <Route exact path="/search" component={Search} />
          <Route exact path="/" component={Search} />
        </Switch>

        <GlobalStyle />
      </Wrapper>
    </ConnectedRouter>
  );
};

App.propTypes = {
  history: PropTypes.object,
};

export default App;
